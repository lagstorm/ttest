#!/usr/bin/env tarantool

-- https://devhub.io/repos/tarantool-http
-- https://habr.com/ru/company/mailru/blog/358706/

json = require('json')

local ERROR_MESSAGES = {
['EMPTY_KEY'] = 'empty key',
['INCORRECT_BODY'] = 'Body is incorrect',
['EMPTY_RESULTS'] = 'empty result',
['KEY_ALREADY_EXISTS'] = 'key already exists',
}
local RESPONSE_CODES = {
['OK'] = 200,
['EMPTY_KEY'] = 400,
['INCORRECT_BODY'] = 400,
['KEY_ALREADY_EXISTS'] = 409,
['EMPTY_RESULTS'] = 404,
['TO_MANY_CONNECTION'] = 429,
}

box.cfg()
log = require('log')
log.info('Load application')
local bootstrap = function()
    log.info('bootstrap')
    box.schema.create_space('kv', {if_not_exists = true})
    box.space.kv:format({
         {name = 'key', type = 'string'},
         {name = 'value', type = 'string'}
    })
	box.space.kv:create_index('primary', {unique = true, if_not_exists = true, parts = { {field = 1, type = 'string'} } })
end
box.once('init_user', bootstrap)

local function rewresp(code, body)
    -- counterOfConnection = counterOfConnection - 1;
    return {
               status = code,
               headers = { ['content-type'] = 'text/json; charset=utf8' },
               body = body
           }
end

-- GET
local function handler_get(self)
    log.info('handler_list is called: req=' .. counterOfConnection)


    log.info('Key is: ' .. self:stash('id'))
    local id = self:stash('id')
    -- ;
    if (nil == id) then
        log.error(ERROR_MESSAGES.EMPTY_KEY)
        return rewresp(RESPONSE_CODES.EMPTY_KEY, ERROR_MESSAGES.EMPTY_KEY)
    end

    local v
    local status, err = pcall(function () v = box.space.kv:get(id) end)
    if (v == nil) then
        
        log.error(ERROR_MESSAGES.EMPTY_RESULTS)
        return rewresp(RESPONSE_CODES.EMPTY_RESULTS, ERROR_MESSAGES.EMPTY_RESULTS)
    end
    
    return  rewresp(RESPONSE_CODES.OK, v[2])
end

--  POST
local function handler_add(self)
    log.info('handler_add is called')
    local status, err = pcall(function () j = self:json() end)
    if (status == false) then
        log.error(ERROR_MESSAGES.INCORRECT_BODY .. err)
        return rewresp(RESPONSE_CODES.INCORRECT_BODY, err)
    end
    if (j.key == nil) then
        log.error(ERROR_MESSAGES.EMPTY_KEY)
        return rewresp(RESPONSE_CODES.EMPTY_KEY, ERROR_MESSAGES.EMPTY_KEY)
    end
    local value = json.encode(j.value)
    local status, err = pcall(function () box.space.kv:insert({j.key, value}) end)
    if (status == false) then
        log.error(ERROR_MESSAGES.KEY_ALREADY_EXISTS .. json.encode(err))
        return rewresp(RESPONSE_CODES.KEY_ALREADY_EXISTS, ERROR_MESSAGES.KEY_ALREADY_EXISTS)
    end
    return rewresp(RESPONSE_CODES.OK, box.space.kv:select(j.key)[2])
end

--  DELETE
local function handler_delete(self)
    log.info('handler_delete is called')
    local id = self:stash('id')
    if (nil == id) then
        
        log.error(ERROR_MESSAGES.EMPTY_KEY)
        return rewresp(RESPONSE_CODES.EMPTY_KEY, ERROR_MESSAGES.EMPTY_KEY)
    end

    local d
    local status, err = pcall(function () d = box.space.kv:get(id) end)
    if (status == false) then
        
        log.error(ERROR_MESSAGES.EMPTY_RESULTS)
        return rewresp(RESPONSE_CODES.EMPTY_RESULTS, err)
    end
    box.space.kv:delete(id)

    
    return rewresp(RESPONSE_CODES.OK, value)
end

--  PUT
local function handler_update(self)
    log.info('handler_update is called')

    local id = self:stash('id')
    if (nil == id) then
        
        log.error(ERROR_MESSAGES.EMPTY_KEY)
        return rewresp(RESPONSE_CODES.EMPTY_KEY, ERROR_MESSAGES.EMPTY_KEY)
    end

    local d
    local status, err = pcall(function () d = box.space.kv:get(id) end)
    if (status == false) then
        
        log.error(ERROR_MESSAGES.EMPTY_RESULTS)
        return rewresp(RESPONSE_CODES.EMPTY_RESULTS, err)
    end

    local j
    local status = pcall(function () j = self:json() end)
    if (status == false) then
        
        log.error(ERROR_MESSAGES.INCORRECT_BODY)
        return rewresp(RESPONSE_CODES.INCORRECT_BODY, ERROR_MESSAGES.INCORRECT_BODY)
    end

    local j = self:json();
    local value = json.encode(j.value)
    box.space.kv:update(id, { {'=', 2, value} })

    
    return rewresp(RESPONSE_CODES.OK, value[2])
end             

  
local httpd = require('http.server')                     
local server = httpd.new('127.0.0.1', 8877, {
    log_requests = true,
    log_errors = true
})

counterOfConnection = 0;

log.info('configure routes')

router = require('http.router').new(options)

local function rps_limiter(req)
    log.info('rps_limiter middleware called')
    if (counterOfConnection > 5) then
        log.error('rps_limiter too many connections')
        return {status = RESPONSE_CODES.TO_MANY_CONNECTION}
    end
    counterOfConnection = counterOfConnection + 1;
    return req:next()
end

router:use(rps_limiter, { name = 'rps_limiter', path = '/kv',  method = 'ANY'})
router:use(rps_limiter, { name = 'rps_limiter', path = '/kv/:id',  method = 'ANY'})


-- ? как корректно зарегистрировать завершающий колбек
local function teardown()
    log.info('teardown middleware called')
    counterOfConnection = counterOfConnection - 1;
end
-- router:use(teardown, { name = 'teardown',  method = 'ANY'})

--


router:route({ path = '/kv', method = 'POST'}, handler_add)
router:route({ path = '/kv/:id', method = 'GET'}, handler_get)
router:route({ path = '/kv/:id', method = 'PUT'}, handler_update)
router:route({ path = '/kv/:id', method = 'DELETE'}, handler_delete)

server:set_router(router)


log.info('begin process')
server:start()
