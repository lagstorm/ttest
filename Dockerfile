FROM tarantool/tarantool:2.2

RUN apk add --no-cache \
        perl \
        gcc \
        g++ \
        cmake \
        readline-dev \
        libressl-dev \
        yaml-dev \
        lz4-dev \
        binutils-dev \
        ncurses-dev \
        lua-dev \
        musl-dev \
        make \
        git \
        libunwind-dev \
        autoconf \
        automake \
        libtool \
        linux-headers \
        go \
        icu-dev \
        wget && \
    git clone https://github.com/tarantool/http.git && \
    cd http && \
    cmake . -DCMAKE_BUILD_TYPE=RelWithDebugInfo && \
    make && \
    make install && \
    cd ../ &&\
    tarantoolctl rocks install http

COPY app/app.lua /opt/tarantool/app.lua

ENTRYPOINT ["docker-entrypoint.sh"]

HEALTHCHECK CMD tarantool_is_up

EXPOSE 3301
CMD ["tarantool"]
