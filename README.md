# run

docker-compose up -d

chmod 666 logs

docker-compose exec tarantool  tarantoolctl restart app


# commands example

curl -X POST -d '{"key":"test","value":{"n":1}}' localhost:8877/kv

curl -X GET 127.0.0.1:8877/kv/test

curl -X PUT -d '{"value":{"n":2}}' localhost:8877/kv/test

curl -X DELETE localhost:8877/kv/test
